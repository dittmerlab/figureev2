# libraries ----
library(gdata)
library(vioplot)
library(ggplot2)
library(reshape2)
library(beeswarm)
library(multcomp)
library(RColorBrewer)
library(lme4)

# options ----
options(digits=3)

getwd()



# functions 

dataCleaning <- function(exosome){
  head(exosome)
  ncol(exosome)
  colnames(exosome)
  colnames(exosome) <- c("Condition","Cell","3","2","1","0","-1","-2","-3")
  
  summary(exosome)
  
  # exosome$DATE <- as.factor(exosome$DATE)
  
  #levels(exosome$Sample)
  #exosome <- exosome[exosome$Sample != "", ]
  
  exosome <- na.omit(exosome) 
  exosome <- drop.levels(exosome)
  
  levels(exosome$Condition)
  levels(exosome$Condition) <- c("parent","EBV")
  exosome$Condition <- relevel(exosome$Condition, ref = "EBV")

  return(exosome)
}
addFloorZero <- function(exosomeAve){
  exosomeAve[exosomeAve$cp <= 0,"cp"] <- 0 
  return(exosomeAve)
}
MultipleComparison <- function(exosome){
  # linear model
  result <- rlm(cp ~ Cell + Condition * Dilution, data = exosome)
  summary(result)
  anova(result)
  pf(q=47.01, df1=5, df2=12, lower.tail=FALSE)
  
  
  # anova
  summary(exosome)
  result <- aov(cp ~ Condition + Cell * Dilution, 
                data = exosome)
  summary(result)
  TukeyHSD(result)$Condition
  
  result.pair <- glht(result, linfct = mcp(Condition="Tukey"))
  summary(result.pair)
  
  result.pair <- glht(result, linfct = mcp(Condition="Dunnett"))
  summary(result.pair)
  confint(result.pair, 
          level = 0.95)
  
  return(result.pair)
}
figure1 <- function(exosome){
  summary(exosome)
  a <-ggplot(aes(x = Dilution, y = cp, color = Condition, shape = Condition), 
             data = exosome) 
  a <- a + geom_point (size = I(5), 
                       alpha = I(0.4))
  
  a <- a + geom_smooth(method = "gam", formula = y ~ poly(x, 3))
  
  a <- a + scale_x_reverse()
  a <- a + scale_colour_brewer(type = "qual", palette = "Set1")
  a <- a + theme_bw() 
  a <- a + xlab ("\n log10(cells/well)") 
  a <- a + ylab ("Fraction positive wells\n") 
  a <- a + labs(title = "Seeding efficiency\n")
  
  # a <- a + scale_y_log10(limits = c(0.1, 1000), breaks=c(0.1,1,10,100,100))
  
  
  a <- a + facet_wrap( ~ Cell)  
  
  a <- a + theme(legend.position = "right") 
  a <- a + theme(strip.background = element_rect(colour = "white", fill = "white"))   
  a <- a + theme(axis.text.x = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.x = element_text(size = 24))
  a <- a + theme(axis.text.y = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.y = element_text(size = 24, angle = 90)) 
  a <- a + theme(plot.title = element_text(size = 24))  
  a <- a  + theme(strip.text.x = element_text(size = 24, vjust = 1))
  a <- a  + theme(strip.text.y = element_text(size = 24))
  a <- a + theme(panel.grid.minor = element_blank()) 
  a <- a + theme(panel.grid.major = element_blank())
  return (a)
}


# DATA READ ----
# 
exosome <- read.delim("Figure_R_serialDilution/experiment2/toR.txt")
summary(exosome)
head(exosome)


# SAFE COPY OF DATASET ----
rainall <- exosome
#write.table(rainall,file = "CompleteDataSet.txt", sep = "\t")
exosome <- rainall


# Data cleaning ----
exosome <- dataCleaning(exosome)
summary(exosome)

exosome <- melt(exosome)

summary(exosome)
colnames(exosome)
colnames(exosome)[3] <- "Dilution"
colnames(exosome)[4] <- "cp"
# exosome$cp <- log10(exosome$cp+1) 

exosome$Dilution <- as.numeric(as.character(exosome$Dilution))
summary(exosome)


#
# Figure 1 ---
print(figure1(exosome))


#png(file = "plot1.png", 
 #   units = "in", 
  #  width = 10, 
   # height = 8, 
    #res = 600)
#  print(figure1(exosome))
#dev.off()


rain <- exosome
rain <- drop.levels(rain)
summary(rain)

# trying to define a cutoff
summary(exosome)
exosome[exosome$cp < 10, "cp"] <- 0
exosome[exosome$cp >= 10, "cp"] <- 1


exosome$Interaction <- interaction(exosome$Condition, exosome$Cell, drop = TRUE)
exosome$Interaction <- interaction(exosome$Interaction, as.factor(exosome$Dilution), drop = TRUE)
exosome$Response <- ave(exosome$cp, 
                        exosome$Interaction, 
                         FUN = sum)

exosome$cp <- exosome$Response/6
print(figure1(exosome))

getwd()
# png(file = "Figure_R_serialDilution/experiment2/Fraction.png", 
#    units = "in",
#    width = 10,
#    height = 8,
#    res = 300)
# print(figure1(exosome))
# dev.off()



# Multiple comparison ----
summary(exosome)
plot(MultipleComparison(exosome))

